import os
import subprocess


def test_run_protoc_as_subprocess():
    import protoc

    rc = subprocess.call(
        [protoc.PROTOC_EXE, "-I.", "-otest.desc", "test.proto"],
        cwd=os.path.dirname(os.path.abspath(__file__)),
    )
    assert rc == 0
