#!/usr/bin/env python3
"""`create_protoc_wheel` is a tool to generate Python wheel packages
for the Google protocol buffer compiler `protoc`.

"""

import argparse
import dataclasses
import hashlib
import os
import shlex
import shutil
import subprocess
import sys
import types
import urllib.request
import zipfile
from typing import Dict, List, Optional, Sequence, Tuple, Type, Union

import rezip
import strictyaml


__version__ = "0.0.20"


PLATFORM_TAGS = [
    "win32",
    "win_amd64",
    "manylinux1_x86_64",
    "manylinux2014_aarch64",
    "macosx_10_6_x86_64",
    "macosx_11_0_arm64",
]


@dataclasses.dataclass
class ProtocInfo:
    """Data about a particular protoc version / platform combination."""

    platform_tag: str = ""
    url: str = ""
    url_prefix: str = ""
    zip_file: str = ""
    version: str = ""
    sha256: Optional[str] = None
    build_number: Optional[str] = None
    timestamp: int = 1609459200  # 2021-01-01 00:00:00 GMT

    def __post_init__(self) -> None:
        if not self.url:
            if not self.url_prefix or not self.zip_file:
                raise ValueError(f"Missing URL in {self}")
            self.url = self.url_prefix + self.zip_file


ProtocInfoDict = Dict[str, Dict[str, ProtocInfo]]


def eprint(*args, file=None, **kwargs):
    """Print to stderr."""
    print(*args, **kwargs, file=sys.stderr)


def load_protoc_versions_yaml(filename: str) -> ProtocInfoDict:
    def normalize_protoc_info(d):
        sha256: Optional[str] = d.get("sha256")
        if sha256 in ("None", "-", "0"):
            sha256 = None
        d["sha256"] = sha256
        return d

    with open(filename, "r", encoding="utf-8") as f:
        yaml_string = f.read()
    protoc_versions_raw = strictyaml.load(yaml_string).data["protoc_versions"]
    return {
        version: {
            platform_tag: ProtocInfo(
                version=version,
                platform_tag=platform_tag,
                timestamp=protoc_version_info["timestamp"],
                url_prefix=protoc_version_info["url_prefix"],
                **normalize_protoc_info(protoc_info),
            )
            for platform_tag, protoc_info in protoc_version_info["platforms"].items()
        }
        for version, protoc_version_info in protoc_versions_raw.items()
    }


def guess_protoc_info(version: str) -> ProtocInfo:
    return {
        platform_tag: ProtocInfo(
            version=version,
            platform_tag=platform_tag,
            url_prefix=f"https://github.com/protocolbuffers/protobuf/releases/download/v{version}/",
            zip_file=f"protoc-{version}-{suffix}.zip",
        )
        for platform_tag, suffix in [
            ("win32", "win32"),
            ("win_amd64", "win64"),
            ("manylinux1_x86_64", "linux-x86_64"),
            ("macosx_10_6_x86_64", "osx-x86_64"),
        ]
    }


def sha256sum(data: bytes) -> str:
    m = hashlib.sha256()
    m.update(data)
    return m.hexdigest()


class CreateProtocWheelConfig(types.SimpleNamespace):
    """Config for create_protoc_wheel"""

    versions: List[str]
    all: bool = False
    verbosity: int = 0
    strict: bool = False

    def __init__(self):
        self.version = []

    @classmethod
    def from_argv(
        cls: Type["CreateProtocWheelConfig"], argv=None
    ) -> "CreateProtocWheelConfig":
        """Create a config from parsing *argv*"""
        if argv is None:
            argv = sys.argv
        p = argparse.ArgumentParser()
        p.add_argument(
            "--quiet",
            "-q",
            dest="verbosity",
            action="store_const",
            const=-1,
            help="Produce less output",
        )
        p.add_argument(
            "--verbose",
            "-v",
            dest="verbosity",
            action="count",
            default=0,
            help="Be more verbose",
        )
        p.add_argument(
            "--all",
            action="store_true",
            help="Create wheels for all supported protoc versions and platforms",
        )
        p.add_argument(
            "--strict",
            action="store_true",
            default=False,
            help="Be strict about checksums",
        )
        p.add_argument("versions", metavar="VERSION", nargs="*")
        args = p.parse_args(argv[1:], namespace=cls())
        assert isinstance(args, cls)
        if args.all:
            if args.versions:
                p.error("it is not support to mix specific versions with --all")
        return args


def slurp_from_url(url: str, verbose: bool = False) -> bytes:
    """Read bytes from *url*"""
    request = urllib.request.Request(url)
    if verbose:
        print(f"Download {url}")
    with urllib.request.urlopen(request) as response:
        return response.read()


def replace_in_bytes(in_bytes: bytes, old: str, new: str) -> bytes:
    """Replace all occurences of *old* by *new* in *in_bytes*"""
    return in_bytes.replace(old.encode(), new.encode())


def copy_and_modify(
    filename_in: str, filename_out: str, replacements: Sequence[Tuple[str, str]]
) -> None:
    """Copy *filename_in* to *filename_out* and apply *replacements*"""
    with open(filename_in, "rb") as f_in:
        contents = f_in.read()
    for old, new in replacements:
        contents = replace_in_bytes(contents, old, new)
    with open(filename_out, "wb") as f_out:
        f_out.write(contents)


def run(
    cmd: Sequence[str], *, cwd: Union[str, os.PathLike, None] = None, verbosity: int = 0
) -> subprocess.CompletedProcess:
    """Run *cmd*"""
    show_cmd = verbosity >= 2
    if show_cmd:
        eprint("+", " ".join(shlex.quote(arg) for arg in cmd))
    try:
        return subprocess.run(
            cmd,
            cwd=cwd,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            check=True,
            encoding="utf-8",
            errors="backslashreplace",
            universal_newlines=True,
        )
    except subprocess.CalledProcessError as exc:
        if not show_cmd:
            eprint("+", " ".join(shlex.quote(arg) for arg in cmd))
        output = exc.output
        if isinstance(output, bytes):
            output = output.decode(encoding="utf-8", errors="backslashreplace")
        else:
            output = str(output)
        eprint(output)
        raise


def create_protoc_wheel(
    protoc_info: ProtocInfo,
    template_dir: str,
    dist_dir: str,
    build_dir: str,
    zip_path: str,
    verbosity: int = 0,
) -> str:
    """Create a protoc wheel file"""
    build_dir = os.path.join(
        build_dir, f"protoc-{protoc_info.version}-{protoc_info.platform_tag}"
    )
    if os.path.exists(build_dir):
        shutil.rmtree(build_dir)
    os.makedirs(build_dir, exist_ok=True)

    with zipfile.ZipFile(zip_path) as protoc_zip:
        protoc_zip.extractall(os.path.join(build_dir, "protoc", "data"))

    if verbosity >= 2:
        print(f"Instantiate template {template_dir} to {build_dir}")
    for dirpath, _dirnames, filenames in os.walk(template_dir):
        rel_dir = os.path.relpath(dirpath, template_dir)
        out_dir = os.path.join(build_dir, rel_dir)
        os.makedirs(out_dir, exist_ok=True)
        for filename in filenames:
            copy_and_modify(
                os.path.join(dirpath, filename),
                os.path.join(out_dir, filename),
                [("@VERSION@", protoc_info.version)],
            )
    build_dist_dir = os.path.join(build_dir, "dist")
    run(
        [
            sys.executable,
            "setup.py",
            "bdist_wheel",
            "--plat-name",
            protoc_info.platform_tag,
            "--dist-dir",
            build_dist_dir,
        ],
        cwd=build_dir,
        verbosity=verbosity,
    )

    dist_files = os.listdir(build_dist_dir)
    if verbosity >= 1:
        print(f"dist_files: {dist_files}")
    if len(dist_files) != 1:
        raise RuntimeError(f"Expected one file in {dist_dir}, got: {dist_files}")
    wheel_in = os.path.join(build_dist_dir, dist_files[0])
    wheel_out = os.path.join(dist_dir, dist_files[0])
    os.makedirs(dist_dir, exist_ok=True)
    if verbosity >= 2:
        print("wheel_in:", wheel_in)
        print("wheel_out:", wheel_out)
    if verbosity < 0:
        rezip_verbosity_args = ["--quiet"]
    elif verbosity == 0:
        rezip_verbosity_args = []
    else:
        rezip_verbosity_args = ["-" + "v" * verbosity]
    rezip.main(
        [
            "rezip",
            *rezip_verbosity_args,
            "--date-time",
            str(protoc_info.timestamp),
            "--normalize-access-rights",
            "--output",
            wheel_out,
            wheel_in,
        ]
    )

    return wheel_out


def create_wheel_for_version(
    version: str,
    *,
    args: CreateProtocWheelConfig,
    protoc_versions: ProtocInfoDict,
    top: str,
    template_dir: str,
    build_dir: str,
    dist_dir: str,
) -> int:

    show_yaml = False

    if version not in protoc_versions:
        if args.strict:
            print(f"Unsupported version {version}", file=sys.stderr)
            return 1
        else:
            protoc_versions[version] = guess_protoc_info(version)
            show_yaml = True

    tag_to_info = protoc_versions[version]
    for _, protoc_info in tag_to_info.items():

        download_path = os.path.join(top, "downloads", protoc_info.zip_file)

        if not os.path.exists(download_path):
            zip_contents = slurp_from_url(protoc_info.url, verbose=True)
            zip_sha256 = sha256sum(zip_contents)
            with open(download_path, "wb") as f_out:
                f_out.write(zip_contents)
            if args.verbosity >= 0:
                print(
                    f"{download_path}: {len(zip_contents)} bytes "
                    f"[from {protoc_info.url}]"
                )
        else:
            with open(download_path, "rb") as f_in:
                zip_sha256 = sha256sum(f_in.read())
            if args.verbosity >= 0:
                print(
                    f"{download_path}: {os.stat(download_path).st_size} bytes "
                    f"[existing file]"
                )

        if protoc_info.sha256 is None:
            show_yaml = True
            protoc_info.sha256 = zip_sha256
        elif zip_sha256 != protoc_info.sha256:
            eprint(f"Checksum mismatch for {download_path}:")
            eprint(f"  expected: {protoc_info.sha256}")
            eprint(f"  got: {zip_sha256}")
            if args.strict:
                return 1
            else:
                show_yaml = True
                protoc_info.sha256 = zip_sha256

        wheel_path = create_protoc_wheel(
            protoc_info,
            template_dir=template_dir,
            build_dir=build_dir,
            dist_dir=dist_dir,
            zip_path=download_path,
            verbosity=args.verbosity,
        )
        if args.verbosity >= 0:
            print(f"Created wheel: {wheel_path}")

    if show_yaml:
        first_info = next(iter(tag_to_info.values()))
        tag_and_info = [
            (tag, tag_to_info.get(tag)) for tag in PLATFORM_TAGS if tag in tag_to_info
        ]
        platforms = {
            tag: {"zip_file": info.zip_file, "sha256": info.sha256}
            for tag, info in tag_and_info
        }
        d = {
            "timestamp": "0",
            "url_prefix": first_info.url_prefix,
            "platforms": platforms,
        }
        print()
        print(f"  {version}:")
        print(f"    # in protobuf repo: git show -s --format=%ct v{version}")
        for line in strictyaml.as_document(d).as_yaml().splitlines():
            print(f"    {line}")
        print()


def main(argv=None):
    """Main program"""
    args = CreateProtocWheelConfig.from_argv(argv=argv)
    top = os.getcwd()

    template_dir = os.path.join(top, "template")
    build_dir = os.path.join(top, "build")
    dist_dir = os.path.join(top, "dist")

    protoc_versions = load_protoc_versions_yaml("protoc_versions.yml")

    if args.all:
        args.versions = list(protoc_versions.keys())

    if args.versions:
        os.makedirs(os.path.join(top, "downloads"), exist_ok=True)

    for version in args.versions:
        ret = create_wheel_for_version(
            version,
            args=args,
            protoc_versions=protoc_versions,
            top=top,
            template_dir=template_dir,
            build_dir=build_dir,
            dist_dir=dist_dir,
        )
        if ret != 0:
            return ret
    return 0


if __name__ == "__main__":
    sys.exit(main())
