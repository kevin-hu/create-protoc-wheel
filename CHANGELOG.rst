===================
 Changelog history
===================


v0.0.20 (2022-06-03)
====================

* Add support for protoc 3.20.1, 21.0 and 21.1


v0.0.19 (2022-04-07)
====================

* Add support for protoc 3.20.0


v0.0.18 (2022-01-29)
====================

* Add support for protoc 3.19.3 and 3.19.4


v0.0.17 (2022-01-07)
====================

* Add support for protoc 3.18.2 and 3.19.2


v0.0.16 (2021-11-09)
====================

* Add support for protoc 3.18.1, 3.19.0 and 3.19.1


v0.0.15 (2021-09-21)
====================

* Add support for protoc 3.18.0


v0.0.14 (2021-06-13)
====================

* Add support for protoc 3.17.3


v0.0.13 (2021-06-07)
====================

* Add support for protoc 3.17.2


v0.0.12 (2021-05-26)
====================

* Add support for protoc 3.17.0 and 3.17.1


v0.0.11 (2021-05-08)
====================

* Add support for protoc 3.16.0


v0.0.10 (2021-04-20)
====================

* Add support for protoc 3.15.8


v0.0.9 (2021-04-04)
===================

* Add support for protoc 3.15.6 and 3.15.7


v0.0.8 (2021-02-27)
===================

* Add support for protoc 3.15.2, 3.15.3, 3.15.4, and 3.15.5


v0.0.7 (2021-02-21)
===================

* Add support for protoc 3.15.1


v0.0.6 (2021-02-19)
===================

* Add support for protoc 3.15.0


v0.0.5 (2020-11-23)
===================

* Add support for protoc 3.13.0 and 3.14.0


v0.0.4 (2020-05-16)
===================

* Add support for protoc 3.11.3, 3.11.4 and 3.12.0


v0.0.1 (2019-05-18)
===================

* Initial release
