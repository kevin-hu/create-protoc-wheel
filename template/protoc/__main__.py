# -*- coding: utf-8; -*-
"""Wrapper to start protoc"""
from __future__ import absolute_import, print_function

import protoc


protoc.exec_protoc()
